setwd("H:/Rclass/testA")
stu.original <- read.csv('stu.csv')
stu<- stu.original

head(stu)
summary(stu)
str(stu)

#Q1
edu <- c("none", "primary edu", "5-9 grade", "secondary edu", "higher edu")
stu$Medu <- factor(stu$Medu, levels = 0:4, labels = edu)
stu$Fedu <- factor(stu$Fedu, levels = 0:4, labels = edu)

stu$id <- NULL

install.packages("ggplot2")
library(ggplot2)

#see if higher affects G3
ggplot(stu, aes(higher,G3)) + geom_boxplot()

#remove rows where tenure=0
filter <- stu$G3 == 0
stu<- stu[!filter,]
ggplot(stu, aes(higher,G3)) + geom_boxplot()


#Q2
#2-A
ggplot(stu, aes(school,G3)) + geom_boxplot()
#2-B
ggplot(stu, aes(absences)) + geom_histogram(binwidth = 1)
ggplot(stu, aes(absences)) + geom_histogram(binwidth = 1)+xlim(0,30)

#2-C

stu$succes<- stu$G3

secc.function <-function(x){ 
  if(x > 11){
    return("passed")
  }
  else{
    return("failed")
  } 
}

stu$succes<- sapply(stu$succes,secc.function)

ggplot(stu) + aes(x = absences , fill = succes) + geom_bar()
ggplot(stu) + aes(x = absences , fill = succes) + geom_bar(position = "fill")
ggplot(stu) + aes(x = absences , fill = succes) + geom_bar(position = "fill")+xlim(0,30)

#2-D

ggplot(stu) + aes(x = failures , fill = succes) + geom_bar()
ggplot(stu) + aes(x = failures , fill = succes) + geom_bar(position = "fill")


#Q3
install.packages('caTools')
library(caTools)

stu1<- stu
stu1$succes<- NULL

modle.filter <- sample.split(stu1$school, SplitRatio = 0.7)
stu1.train <- subset(stu1, modle.filter == TRUE)
stu1.test <- subset(stu1, modle.filter == FALSE)

reg.model <- lm(G3~., stu1.train)

summary(reg.model)

predicted.test <- predict(reg.model, stu1.test)
MSE.rgtest <- mean((stu1.test$G3-predicted.test)**2)
RMSE <- MSE.rgtest**0.5


#Q3- c
reg.predict<-predicted.test
stu.check<-stu1.test
stu.check$predict<- reg.predict
str(stu.check)

accept.function <-function(x){ 
  if(x >= 11){
    return("accepted")
  }
  else{
    return("didn't accepted")
  } 
}

stu.check$accepted<-sapply(stu.check$predict,accept.function) 

stu.check$accepted<- as.factor(stu.check$accepted)
summary(stu.check)

accept.filter <- stu.check$accepted == "didn't accepted"
stu.check1<- stu.check
stu.check1<- stu.check1[!accept.filter,]
summary(stu.check1)

secc1.function <-function(x){ 
  if(x >= 11){
    return("passed")
  }
  else{
    return("failed")
  } 
}
stu.check1$succes<-stu.check1$G3
stu.check1$succes<- sapply(stu.check1$succes,secc1.function)
stu.check1$succes<- as.factor(stu.check1$succes)
summary(stu.check1)

stu.check.original<-stu1.test
stu.check.original$succes<-stu.check.original$G3
stu.check.original$succes<- sapply(stu.check.original$succes,secc1.function)
stu.check.original$succes<- as.factor(stu.check.original$succes)
summary(stu.check.original)


#4
stu2<-stu
str(stu2)
stu2$succes<- NULL
head(stu2)

sort.stu2<-stu2[order(stu2$G3) , ]
head(sort.stu2)
str(sort.stu2)

sort.stu2[[17]][[286]] #=15

sort.stu2$Outstan<- sort.stu2$G3

outsta.function <-function(x){ 
  if(x >= 11){
    return(TRUE)
  }
  else{
    return(FALSE)
  } 
}
sort.stu2$Outstan<- sapply(sort.stu2$Outstan,outsta.function)

modle2.filter <- sample.split(sort.stu2$school, SplitRatio = 0.7)
sort.stu2.train <- subset(sort.stu2, modle2.filter == TRUE)
sort.stu2.test <- subset(sort.stu2, modle2.filter == FALSE)


install.packages('e1071')
library(e1071)

dim(sort.stu2.train)
dim(sort.stu2.test)

model2 <- naiveBayes(sort.stu2.train[,-18],sort.stu2.train$Outstan)



prediction.naiv <- predict(model2, sort.stu2.test[,-18], type = 'raw')
predict.outsta <- prediction.naiv[,'TRUE']
predicted.naiv <- predict.outsta > 0.5
actual <- sort.stu2.test$Outstan
confusion.matrix <- table(predicted.naiv, actual)

precision <- confusion.matrix['TRUE','TRUE']/(confusion.matrix['TRUE','TRUE']+confusion.matrix['FALSE','TRUE'])
recall <- confusion.matrix['TRUE','TRUE']/(confusion.matrix['TRUE','TRUE']+confusion.matrix['FALSE','FALSE'])


